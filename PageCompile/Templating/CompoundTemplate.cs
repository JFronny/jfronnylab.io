using System.Collections.Generic;
using System.Linq;
using PageCompile.Logging;

namespace PageCompile.Templating;

public class CompoundTemplate : Template
{
    public readonly List<Template> Templates = [];

    protected override string ApplyInternal(string source, Logger logger)
    {
        return Templates.Aggregate(source, (s, template) => template.Apply(s, logger.Level()));
    }
}