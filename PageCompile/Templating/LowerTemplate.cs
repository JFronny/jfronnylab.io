using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PageCompile.Templating;

public class LowerTemplate : DictTemplate
{
    public LowerTemplate(Template template, ConstantsTemplate constantsTemplate, string[] pgIgnore)
    {
        _pgIgnore = pgIgnore;
        Add("separator", (s, l) => string.IsNullOrWhiteSpace(s) ? "" : $" - {template.Apply(s, l)}");
        Add("name", (s, l) =>
        {
            s = template.Apply(s, l);
            if (s == "..")
                return "..";
            s = Path.GetFileNameWithoutExtension(s);
            if (Directory.Exists(s))
                return s + "/";
            return s;
        });
        Add("relative", (s, l) =>
        {
            if (s == "..")
                return "..";
            string[] paths = template.Apply(s, l).Split(',');
            string second = paths[0];
            string basePath = paths.Length > 1 ? paths[1] : Path.GetDirectoryName(constantsTemplate.Path)!;
            l.Info(basePath);
            l.Info(second);
            string relative = Path.GetRelativePath(basePath, second);
            l.Info($"Converted absolute path {second} to relative path {relative} using {basePath}");
            return relative;
        });
        Add("parent", (s, l) =>
        {
            if (s == "..")
                return "../..";
            if (s == "." || string.IsNullOrWhiteSpace(s))
                return "..";
            return Path.GetDirectoryName(s);
        });
        Add("cd", (s, l) => string.IsNullOrWhiteSpace(s) ? GetDir(SourcePath, !IsRoot) : GetDir(Path.GetFullPath(s, SourcePath), false));
    }

    private string GetDir(string path, bool hasUp)
    {
        IEnumerable<string> files = Directory.GetFiles(path).Except(new[] { "./template.html" }).ToArray();
        List<string> result = [];
        if (hasUp) result.Add("..");
        if (files.Any(s => Path.GetFileName(s) == "index.html"))
            result.Add(files.First(s => Path.GetFileName(s) == "index.html"));
        result.AddRange(Directory.GetDirectories(path).Where(s => !FileIsResource(s)));
        result.AddRange(files.Where(s => Path.GetFileName(s) != "index.html" && s != "./.pgResource" && !FileIsResource(s)));
        return string.Join(' ', result);
    }

    public string SourcePath = "";
    public bool IsRoot = false;
    private readonly string[] _pgIgnore;

    public bool FileIsResource(string path) => _pgIgnore.Contains(path) || _pgIgnore.Contains(Path.GetFileName(path));
}