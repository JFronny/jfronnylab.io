namespace PageCompile.Templating;

public class UpperTemplate : DictTemplate
{
    public UpperTemplate(Template template, ConstantsTemplate constantsTemplate)
    {
        Add("devAlt", (s, l) =>
        {
            int index = s.IndexOf(',');
            if (index == -1)
            {
                l.Fail($"Could not find comma in devAlt param \"{s}\"");
                return s;
            }
#if DEBUG
            return template.Apply(s.Substring(index + 1), l);
#else
            return template.Apply(s.Substring(0, index), l);
#endif
        });
        Add("foreach", (s, l) =>
        {
            int index = s.IndexOf(',');
            if (index == -1)
            {
                l.Fail($"Could not find comma in foreach param \"{s}\"");
                return s;
            }
            string source = s.Substring(index + 1);
            string result = "";
            foreach (string s1 in template.Apply(s.Substring(0, index), l).Split(' '))
            {
                constantsTemplate.It = s1;
                result += template.Apply(source, l);
            }
            return result.Trim(' ');
        });
    }
}