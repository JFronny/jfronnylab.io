using PageCompile.Logging;

namespace PageCompile.Templating;

public abstract class Template
{
    protected abstract string ApplyInternal(string source, Logger logger);

    public string Apply(string source, Logger logger)
    {
        string prev;
        do
        {
            prev = source;
            source = ApplyInternal(source, logger);
        } while (source != prev);
        return source;
    }
}