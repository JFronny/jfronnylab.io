using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PageCompile.Logging;

namespace PageCompile.Templating;

public class DictTemplate : Template, IDictionary<string, Func<string, Logger, string>>
{
    private readonly IDictionary<string, Func<string, Logger, string>> _backingDict = new Dictionary<string, Func<string, Logger, string>>();
    public IEnumerator<KeyValuePair<string, Func<string, Logger, string>>> GetEnumerator() => _backingDict.GetEnumerator();
    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    public void Add(KeyValuePair<string, Func<string, Logger, string>> item) => _backingDict.Add(item);
    public void Clear() => _backingDict.Clear();
    public bool Contains(KeyValuePair<string, Func<string, Logger, string>> item) => _backingDict.Contains(item);
    public void CopyTo(KeyValuePair<string, Func<string, Logger, string>>[] array, int arrayIndex) => _backingDict.CopyTo(array, arrayIndex);
    public bool Remove(KeyValuePair<string, Func<string, Logger, string>> item) => _backingDict.Remove(item);
    public int Count => _backingDict.Count;
    public bool IsReadOnly => _backingDict.IsReadOnly;
    public void Add(string key, Func<string, Logger, string> value) => _backingDict.Add(key, value);
    public bool ContainsKey(string key) => _backingDict.ContainsKey(key);
    public bool Remove(string key) => _backingDict.Remove(key);
    public bool TryGetValue(string key, out Func<string, Logger, string> value) => _backingDict.TryGetValue(key, out value);
    public Func<string, Logger, string> this[string key]
    {
        get => _backingDict[key];
        set => _backingDict[key] = value;
    }
    public ICollection<string> Keys => _backingDict.Keys;
    public ICollection<Func<string, Logger, string>> Values => _backingDict.Values;

    protected override string ApplyInternal(string source, Logger logger)
    {
        foreach ((string key, Func<string, Logger, string> value) in _backingDict.OrderByDescending(s => s.Key.Length))
        {
            string keyStringStart = $"${{{key}";
            string keyStringFull = $"{keyStringStart}}}";
            if (source.Contains(keyStringFull))
            {
                logger.Info($"Applying {key} for un-parameterized invokes");
                source = source.Replace(keyStringFull, value("", logger.Level()));
            }
            if (source.Contains(keyStringStart))
            {
                logger.Info($"Applying {key} for parameterized invokes");
                while (true)
                {
                    int index = source.IndexOf(keyStringStart, StringComparison.Ordinal);
                    if (index == -1) break;
                    int endIndex = FindClosing(source, index + 1);
                    if (endIndex == -1)
                    {
                        logger.Fail("Could not find param ending");
                        break;
                    }
                    string param = source.Substring(index, endIndex - index);
                    param = param.Substring(param.IndexOf(':') == -1 ? key.Length : param.IndexOf(':') + 1);
                    logger.Info($"Identified param \"{param}\"");
                    source = source.Substring(0, index) + value(param, logger.Level()) + source.Substring(endIndex + 1);
                }
            }
        }
        return source;
    }

    private int FindClosing(string text, int opening)
    {
        int count = 0;
        for (int i = 0; i < text.Substring(opening).Length; i++)
        {
            char c = text.Substring(opening)[i];
            if (c == '{') count++;
            if (c == '}') count--;
            if (count == 0) return opening + i;
        }
        return -1;
    }
}