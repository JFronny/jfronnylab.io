using System.Collections.Generic;
using System.Linq;
using PageCompile.Logging;

namespace PageCompile.Templating;

public class ConstantsTemplate : Template
{
    private readonly Dictionary<string, string> _constants;

    public ConstantsTemplate() => _constants = new Dictionary<string, string> {{"page", ""}, {"path", ""}, {"pageTitle", ""}, {"it", ""}};

    public string Page
    {
        get => _constants["page"];
        set => _constants["page"] = value;
    }
        
    public string PageTitle
    {
        get => _constants["pageTitle"];
        set => _constants["pageTitle"] = value;
    }
        
    public string Path
    {
        get => _constants["path"];
        set => _constants["path"] = value;
    }
        
    public string It
    {
        get => _constants["it"];
        set => _constants["it"] = value;
    }

    protected override string ApplyInternal(string source, Logger logger)
    {
        foreach ((string key, string value) in _constants.OrderByDescending(s => s.Key.Length))
        {
            string keyStringStart = $"${{{key}";
            string keyStringFull = $"{keyStringStart}}}";
            if (source.Contains(keyStringFull))
            {
                logger.Info($"Applying {key} for un-parameterized invokes");
                source = source.Replace(keyStringFull, value);
            }
        }
        return source;
    }
}