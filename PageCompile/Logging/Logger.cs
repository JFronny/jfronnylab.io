namespace PageCompile.Logging;

public abstract class Logger
{
    public abstract void Info(string text);
    public abstract void Warn(string text);
    public abstract void Fail(string text);
    public Logger Level() => new SubLogger(this);

    private class SubLogger : Logger
    {
        private readonly Logger _baseLogger;

        public SubLogger(Logger baseLogger) => _baseLogger = baseLogger;

        public override void Info(string text) => _baseLogger.Info($"| {text}");
        public override void Warn(string text) => _baseLogger.Warn($"| {text}");
        public override void Fail(string text) => _baseLogger.Fail($"| {text}");
    }
}