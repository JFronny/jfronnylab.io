using System;

namespace PageCompile.Logging;

public class ConsoleLogger : Logger
{
    public override void Info(string text)
    {
        Console.ResetColor();
        Console.WriteLine(text);
    }

    public override void Warn(string text)
    {
        Console.ResetColor();
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Error.WriteLine(text);
        Console.ResetColor();
    }

    public override void Fail(string text)
    {
        Console.ResetColor();
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Error.WriteLine(text);
        Console.ResetColor();
    }
}