﻿using System;
using System.IO;
using PageCompile.Logging;
using PageCompile.Templating;

namespace PageCompile;

internal static class Program
{
    private static void Main(string[] args)
    {
        Logger logger = new ConsoleLogger();
        if (args.Length != 2)
        {
            logger.Fail("Incorrect parameters");
            return;
        }
        string src = args[0];
        string trg = args[1];
        if (!Directory.Exists(src))
        {
            logger.Fail("Source dir not found");
            return;
        }
        src = Path.GetFullPath(src);
        if (!Directory.Exists(trg))
        {
            logger.Fail("Target dir not found");
            return;
        }
        trg = Path.GetFullPath(trg);
        Environment.CurrentDirectory = src;
        string source = "template.html";
        if (!File.Exists(source))
        {
            logger.Fail("Template file not found");
            return;
        }
        source = File.ReadAllText(source);
        Directory.Delete(trg, true);
        Directory.CreateDirectory(trg);

        string[] pgResource = [];
        if (File.Exists(".pgResource"))
            pgResource = File.ReadAllLines(".pgResource");
        else
            logger.Warn("Could not find .pgResource, assuming universal processing");

        CompoundTemplate template = new();
        ConstantsTemplate constantsTemplate = new();
        UpperTemplate upperTemplate = new(template, constantsTemplate);
        LowerTemplate lowerTemplate = new(template, constantsTemplate, pgResource);

        template.Templates.Add(upperTemplate);
        template.Templates.Add(constantsTemplate);
        template.Templates.Add(lowerTemplate);

        BuildDir(template,
            lowerTemplate,
            constantsTemplate,
            logger,
            ".",
            trg,
            source,
            true);
    }

    private static void BuildDir(Template template, LowerTemplate lowerTemplate, ConstantsTemplate contentTemplate, Logger logger, string sourcePath, string targetPath, string source, bool isRoot)
    {
        lowerTemplate.SourcePath = sourcePath;
        lowerTemplate.IsRoot = isRoot;
        logger.Info("Setting up default index page in " + targetPath);
        contentTemplate.Page = "";
        contentTemplate.Path = Path.Combine(sourcePath, "index.html");
        contentTemplate.PageTitle = "";
        File.WriteAllText(Path.Combine(targetPath, "index.html"), template.Apply(source, logger.Level()));
        // Build files
        foreach (string file in Directory.GetFiles(sourcePath))
        {
            string newTarget = Path.Combine(targetPath, Path.GetFileName(file));
            if (file == "./template.html" || file == "./.pgResource")
                continue;
            if (lowerTemplate.FileIsResource(file) || Path.GetExtension(file) != ".html")
            {
                logger.Info("Copying " + newTarget);
                File.Copy(file, newTarget, true);
            }
            else
            {
                logger.Info("Building " + newTarget);
                contentTemplate.Page = File.ReadAllText(file);
                contentTemplate.Path = file;
                contentTemplate.PageTitle = sourcePath + "/" + Path.GetFileNameWithoutExtension(file);
                File.WriteAllText(newTarget, template.Apply(source, logger.Level()));
            }
        }
        // Build subdirs
        foreach (string directory in Directory.GetDirectories(sourcePath))
        {
            string newTarget = Path.Combine(targetPath, Path.GetFileName(directory));
            if (lowerTemplate.FileIsResource(directory))
            {
                logger.Info("Copying " + newTarget);
                CopyRecursively(directory, newTarget);
            }
            else
            {
                logger.Info("Discovered " + newTarget);
                if (!Directory.Exists(newTarget))
                    Directory.CreateDirectory(newTarget);
                BuildDir(template, lowerTemplate, contentTemplate, logger.Level(), directory, newTarget, source, false);
            }
        }
    }

    private static void CopyRecursively(string sourcePath, string targetPath)
    {
        Directory.CreateDirectory(targetPath);
        //Copy all the files & Replaces any files with the same name
        foreach (string newPath in Directory.GetFiles(sourcePath))
        {
            File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
        }

        //Now Create all of the directories
        foreach (string dirPath in Directory.GetDirectories(sourcePath))
        {
            CopyRecursively(sourcePath, targetPath);
        }
    }
}