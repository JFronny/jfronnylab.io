Contact: mailto:projects.security@frohnmeyer-wds.de
Expires: 2030-12-31T22:59:00.000Z
Encryption: https://jfronny.gitlab.io/keys/email.txt
Preferred-Languages: en, de
Canonical: https://jfronny.gitlab.io/.well-known/security.txt
