using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
    
WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
builder.Services.AddCors();
builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "JfAPI", Version = "v1" });
});

WebApplication app = builder.Build();
if (app.Environment.IsDevelopment()) app.UseDeveloperExceptionPage();
app.UseSwagger(c =>
{
    c.RouteTemplate = "jfapi/swagger/{documentName}/swagger.json";
});
app.UseSwaggerUI(c =>
{
    c.DocumentTitle = "JfAPI Swagger";
    c.RoutePrefix = "jfapi/swagger";
    c.SwaggerEndpoint("/jfapi/swagger/v1/swagger.json", "JfApi v1");
});
#if FORCE_SSL
app.UseHttpsRedirection();
#endif
app.UseRouting();
app.UseCors(x => x.AllowAnyMethod().AllowAnyHeader().SetIsOriginAllowed(_ => true).AllowCredentials());
app.MapControllers();
app.Run();