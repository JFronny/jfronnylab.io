using System;
using System.Net.Http;

namespace Backend;

public static class Shared
{
    public const string API_BACKEND =
#if DEBUG
            "https://localhost:5003/jfapi"
#else
            "https://akbvopenfl1.hopto.org/jfapi"
#endif
        ;
    public static readonly HttpClient Hc = new();
    public static readonly Random Rng = new();

    static Shared()
    {
        Hc.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36");
    }
}