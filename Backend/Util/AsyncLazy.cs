using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Backend.Util;

public class AsyncLazy<T>
{
    private readonly Func<Task<T>> _supplier;
    private Optional<T> _value = new();
    public AsyncLazy(Func<Task<T>> supplier) => _supplier = supplier;

    private async Task<T> Get() => _value.HasValue ? _value : _value = await _supplier();

    public TaskAwaiter<T> GetAwaiter() => Get().GetAwaiter();
}