using System.Net;
using System.Text.RegularExpressions;

namespace Backend.Util;

public static class DataUtil
{
    public static string CleanPostMessage(string message)
    {
        if (string.IsNullOrEmpty(message)) return message;
        message = message.Replace("<br>", "\n");
        message = Regex.Replace(message, "<.+?>", "");
        message = WebUtility.HtmlDecode(message).Trim();
        return message;
    }
}