using System;

namespace Backend.Util;

public class Optional<T>
{
    public  bool HasValue { get; }
    private readonly T _value;

    public Optional()
    {
    }

    private Optional(T value)
    {
        _value = value;
        HasValue = true;
    }

    public static implicit operator T(Optional<T> optional)
    {
        if (optional.HasValue) return optional._value;
        else throw new InvalidOperationException();
    }
        
    public static implicit operator Optional<T>(T value) => new(value);

    public override bool Equals(object obj)
    {
        if (obj is Optional<T> o)
            if (HasValue && o.HasValue) return Equals(_value, o._value);
            else return HasValue == o.HasValue;
        else return false;
    }

    public override int GetHashCode() => HasValue ? _value.GetHashCode() : 0;
    public override string ToString() => HasValue ? _value.ToString() : "";
}