using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Backend.Util;

public class AsyncLazyDictExpiring<TKey, TValue>
{
    private readonly Func<TKey, Task<TValue>> _supplier;
    private readonly Dictionary<TKey, Tuple<DateTime, TValue>> _cache = new();

    public AsyncLazyDictExpiring(Func<TKey, Task<TValue>> supplier) => _supplier = supplier;

    private async Task<TValue> Get(TKey key)
    {
        if (!_cache.ContainsKey(key) 
            || DateTime.Now - _cache[key].Item1 > new TimeSpan(0, 5, 0))
            _cache[key] = new Tuple<DateTime, TValue>(DateTime.Now, await _supplier(key));
        return _cache[key].Item2;
    }

    public Task<TValue> this[TKey key] => Get(key);
}