using System;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace Backend.Util;

public class AsyncLazyExpiring<T>
{
    private Optional<T> _cache = new();
    private DateTime _cacheTime = new(2000, 1, 1);
    private readonly Func<Task<T>> _supplier;
    public AsyncLazyExpiring(Func<Task<T>> supplier) => _supplier = supplier;

    private async Task<T> Get()
    {
        if (!_cache.HasValue || DateTime.Now - _cacheTime > new TimeSpan(0, 5, 0))
        {
            T val = await _supplier();
            _cache = val;
            _cacheTime = DateTime.Now;
        }
        return _cache;
    }

    public TaskAwaiter<T> GetAwaiter() => Get().GetAwaiter();
}