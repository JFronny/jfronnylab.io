using System.Text.Json.Serialization;
using Backend.Model.Ingest;

namespace Backend.Model;

public class BoardInfoExport
{
    private readonly BoardInfoIngest _ingest;
    public BoardInfoExport(BoardInfoIngest ingest) => _ingest = ingest;

    [JsonPropertyName("board")] public string ShortName => _ingest.ShortName;
    [JsonPropertyName("title")] public string Title => _ingest.Title;
    [JsonPropertyName("workSafe")] public bool Worksafe => _ingest.Worksafe == 1;
    [JsonPropertyName("perPage")] public int ThreadsPerPage => _ingest.ThreadsPerPage;
    [JsonPropertyName("pages")] public int PagesCount => _ingest.PagesCount;
    [JsonPropertyName("maxFilesize")] public int MaxFilesize => _ingest.MaxFilesize;
    [JsonPropertyName("maxWebmFilesize")] public int MaxWebMFilesize => _ingest.MaxWebMFilesize;
    [JsonPropertyName("maxCommentChars")] public int MaxCommentLength => _ingest.MaxCommentLength;
    [JsonPropertyName("bumpLimit")] public int BumpLimit => _ingest.BumpLimit;
    [JsonPropertyName("cooldowns")] public CooldownInfoIngest Cooldowns => _ingest.Cooldowns;
    [JsonPropertyName("spoilers")] public bool SpoilersEnabled => _ingest.SpoilersEnabled == 1;
    [JsonPropertyName("customSpoilers")] public int CustomSpoilersCount => _ingest.CustomSpoilersCount;
    [JsonPropertyName("userIds")] public bool UserIdsEnabled => _ingest.UserIdsEnabled == 1;
    [JsonPropertyName("codeTags")] public bool CodeTagsEnabled => _ingest.CodeTagsEnabled == 1;
    [JsonPropertyName("countryFlags")] public bool CountryFlagsEnabled => _ingest.CountryFlagsEnabled == 1;
    [JsonPropertyName("mathTags")] public bool MathTagsEnabled => _ingest.MathTagsEnabled == 1;
}