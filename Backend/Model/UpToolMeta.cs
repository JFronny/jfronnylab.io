namespace Backend.Model;

public class UpToolMeta
{
    public readonly File Portable;
    public readonly File InstallerCli;
    public readonly File InstallerGui;
    public readonly string Version;

    public UpToolMeta(File portable, File installerCli, File installerGui, string version)
    {
        Portable = portable;
        InstallerCli = installerCli;
        InstallerGui = installerGui;
        Version = version;
    }

    public class File
    {
        public readonly string Name;
        public readonly string Url;
        public readonly string Hash;

        public File(string url, string hash, string name)
        {
            Url = url;
            Hash = hash;
            Name = name;
        }
    }
}