using System;
using System.Net;
using System.Text.Json.Serialization;
using Backend.Model.Ingest;
using Backend.Util;

namespace Backend.Model;

public class ThreadModelExport
{
    [JsonIgnore] private readonly ThreadStarterModelIngest _ingest;
    [JsonIgnore] private readonly string _board;
    public ThreadModelExport(ThreadStarterModelIngest ingest, string board)
    {
        _ingest = ingest;
        _board = board;
    }

    [JsonPropertyName("board")] public string Board => _board;
    [JsonPropertyName("postNumber")] public int PostNumber => _ingest.no;
    [JsonPropertyName("name")] public string Name => _ingest.name;
    [JsonPropertyName("message")] public string Message => DataUtil.CleanPostMessage(_ingest.com);
    [JsonPropertyName("image")] public ImageModelExport Image => string.IsNullOrEmpty(_ingest.ext) ? null : new ImageModelExport(_ingest, _board);
    [JsonPropertyName("timeCreated")] public DateTime TimeCreated => new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
        .Add(TimeSpan.FromSeconds(_ingest.time));
    [JsonPropertyName("trip")] public string Trip => _ingest.trip;
    [JsonPropertyName("subject")] public string Subject => WebUtility.HtmlDecode(_ingest.sub ?? "");
    [JsonPropertyName("sticky")] public bool Sticky => _ingest.sticky == 1;
    [JsonPropertyName("closed")] public bool Closed => _ingest.closed == 1;
}

public class ImageModelExport
{
    [JsonIgnore] private readonly ThreadStarterModelIngest _ingest;
    [JsonIgnore] private readonly string _board;
    public ImageModelExport(ThreadStarterModelIngest ingest, string board)
    {
        _ingest = ingest;
        _board = board;
    }

    [JsonPropertyName("width")] public int Width => _ingest.w;
    [JsonPropertyName("height")] public int Height => _ingest.h;
    [JsonPropertyName("filename")] public string Filename => _ingest.filename;
    [JsonPropertyName("md5Hash")] public string Md5Hash => _ingest.md5;
    [JsonPropertyName("filesize")] public int Filesize => _ingest.fsize;
    [JsonPropertyName("image")] public Uri Image => new($"https://i.4cdn.org/{_board}/{_ingest.tim}{_ingest.ext}");
    [JsonPropertyName("thumbnail")] public Uri Thumbnail => new($"{Shared.API_BACKEND}/v2/chan/image?board={_board}&image={_ingest.tim}");
}