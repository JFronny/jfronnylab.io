using System;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Backend.Model.Ingest;

public static class IpInfo
{
    public static async Task<IpInfoData> Get(string ip)
    {
        Console.WriteLine(ip);
        using HttpRequestMessage request = new(HttpMethod.Get, $"https://ipinfo.io/widget/demo/{ip}");
        request.Headers.Add("referer", "https://ipinfo.io/");
        using HttpResponseMessage response = await Shared.Hc.SendAsync(request);
        using HttpContent content = response.Content;
        Console.WriteLine(await content.ReadAsStringAsync());
        return (await JsonSerializer.DeserializeAsync<Response>(await content.ReadAsStreamAsync()))!.Data!;
    }

    public class Response
    {
        [JsonPropertyName("input")] public string? Input { get; set; }
        [JsonPropertyName("data")] public IpInfoData? Data { get; set; }
    }
    
    public class IpInfoData
    {
        [JsonPropertyName("ip")] public string? Ip { get; set; }
        [JsonPropertyName("hostname")] public string? Hostname { get; set; }
        [JsonPropertyName("city")] public string? City { get; set; }
        [JsonPropertyName("region")] public string? Region { get; set; }
        [JsonPropertyName("country")] public string? Country { get; set; }
        [JsonPropertyName("loc")] public string? Coordinates { get; set; }
        [JsonPropertyName("org")] public string? Organization { get; set; }
        [JsonPropertyName("postal")] public string? PostalCode { get; set; }
        [JsonPropertyName("timezone")] public string? Timezone { get; set; }
        [JsonPropertyName("asn")] public Asn? Asn { get; set; }
        [JsonPropertyName("company")] public Company? Company { get; set; }
        [JsonPropertyName("privacy")] public Privacy? Privacy { get; set; }
        [JsonPropertyName("abuse")] public Abuse? Abuse { get; set; }
    }

    public class Asn
    {
        [JsonPropertyName("asn")] public string? Id { get; set; }
        [JsonPropertyName("name")] public string? Name { get; set; }
        [JsonPropertyName("domain")] public string? Domain { get; set; }
        [JsonPropertyName("route")] public string? Route { get; set; }
        [JsonPropertyName("type")] public string? Type { get; set; }
    }

    public class Company
    {
        [JsonPropertyName("name")] public string? Name { get; set; }
        [JsonPropertyName("domain")] public string? Domain { get; set; }
        [JsonPropertyName("type")] public string? Type { get; set; }
    }

    public class Privacy
    {
        [JsonPropertyName("vpn")] public bool Vpn { get; set; }
        [JsonPropertyName("proxy")] public bool Proxy { get; set; }
        [JsonPropertyName("tor")] public bool Tor { get; set; }
        [JsonPropertyName("relay")] public bool Relay { get; set; }
        [JsonPropertyName("hosting")] public bool Hosting { get; set; }
        [JsonPropertyName("service")] public string? Service { get; set; }
    }

    public class Abuse
    {
        [JsonPropertyName("address")] public string? Address { get; set; }
        [JsonPropertyName("country")] public string? Country { get; set; }
        [JsonPropertyName("email")] public string? Email { get; set; }
        [JsonPropertyName("name")] public string? Name { get; set; }
        [JsonPropertyName("network")] public string? Network { get; set; }
        [JsonPropertyName("phone")] public string? Phone { get; set; }
    }
}