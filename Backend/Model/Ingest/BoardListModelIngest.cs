using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Backend.Model.Ingest;

public class BoardListModelIngest
{
    public List<BoardInfoIngest> boards { get; set; }
}

public class BoardInfoIngest
{
    [JsonPropertyName("board")]
    public string ShortName { get; set; }

    [JsonPropertyName("title")]
    public string Title { get; set; }

    [JsonPropertyName("ws_board")]
    public int Worksafe { get; set; }

    [JsonPropertyName("per_page")]
    public int ThreadsPerPage { get; set; }

    [JsonPropertyName("pages")]
    public int PagesCount { get; set; }

    [JsonPropertyName("max_filesize")]
    public int MaxFilesize { get; set; }

    [JsonPropertyName("max_webm_filesize")]
    public int MaxWebMFilesize { get; set; }

    [JsonPropertyName("max_comment_chars")]
    public int MaxCommentLength { get; set; }

    [JsonPropertyName("bump_limit")]
    public int BumpLimit { get; set; }

    [JsonPropertyName("cooldowns")]
    public CooldownInfoIngest Cooldowns { get; set; }

    [JsonPropertyName("spoilers")]
    public int SpoilersEnabled { get; set; }

    [JsonPropertyName("custom_spoilers")]
    public int CustomSpoilersCount { get; set; }

    [JsonPropertyName("user_ids")]
    public int UserIdsEnabled { get; set; }

    [JsonPropertyName("code_tags")]
    public int CodeTagsEnabled { get; set; }

    [JsonPropertyName("country_flags")]
    public int CountryFlagsEnabled { get; set; }

    [JsonPropertyName("math_tags")]
    public int MathTagsEnabled { get; set; }
}

public class CooldownInfoIngest
{
    [JsonPropertyName("threads")]
    public int Thread { get; set; }

    [JsonPropertyName("replies")]
    public int Reply { get; set; }

    [JsonPropertyName("images")]
    public int Images { get; set; }
}