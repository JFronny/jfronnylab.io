using System.IO;
using System.Text;
using System.Threading.Tasks;
using Backend.Model;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers;

[ApiController]
[Route("jfapi/[controller]")]
public class RouletteController : ControllerBase
{
    [HttpGet]
    public async Task<ActionResult> Get()
    {
        string board = GetRandom(await ChanController1.BoardsCache).ShortName;
        ThreadModelExport th = new(GetRandom(await ChanController1.CatalogCache[board]), board);
        string html = $@"<!DOCTYPE html>
<html lang=""en"">
<head>
    <meta charset=""UTF-8"">
    <title>JFronny - Roulette</title>
    <link rel=""stylesheet"" href=""https://jfronny.gitlab.io/style.css"">
    <style>
        @font-face {{ font-family: JetBrains Mono NL; src: url('https://jfronny.gitlab.io/font.ttf'); }}
    </style>
</head
<body>
<div class=""body"">
    <div class=""segment"">
        <h1>Roulette</h1>
        <a href=""https://jfronny.gitlab.io/projects"" class=""navElem"">..</a>
    </div>
    <br>
    <div class=""segment"" style=""text-align: center;"">
        <a href=""./roulette"">Next</a>
        <h3>
            <a target=""_blank"" rel=""noopener noreferrer"" href=""https://boards.4channel.org/{th.Board}"">/{th.Board}/</a>
            -
            {th.Name}:
            <a target=""_blank"" rel=""noopener noreferrer"" href=""https://boards.4channel.org/{th.Board}/thread/{th.PostNumber}"">
            {(string.IsNullOrWhiteSpace(th.Subject) ? "Untitled" : th.Subject)}
            </a>
        </h5>
        <a href=""{th.Image.Image}""><img style=""height:300px;object-fit: contain;"" src=""{th.Image.Thumbnail}"" alt=""Loading image""></a>
        <p>
            {th.Message}
        </p>
    </div>
</div>
</html>";
            
        return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(html)), "text/html");
    }

    private T GetRandom<T>(T[] array) => array[Shared.Rng.Next(array.Length)];
}