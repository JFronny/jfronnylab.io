using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Backend.Model;
using Backend.Model.Ingest;
using Backend.Util;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers;

[ApiController]
[Route("jfapi/v2/chan", Name = "Chan v1")]
public class ChanController1 : ControllerBase
{
    public static readonly AsyncLazyExpiring<BoardInfoIngest[]> BoardsCache = new(async () =>
    {
        string s = await Shared.Hc.GetStringAsync("https://a.4cdn.org/boards.json");
        return JsonSerializer.Deserialize<BoardListModelIngest>(s)!.boards.ToArray();
    });
        
    public static readonly AsyncLazyDictExpiring<string, ThreadStarterModelIngest[]> CatalogCache = new(async (board) =>
    {
        string s = await Shared.Hc.GetStringAsync($"https://a.4cdn.org/{board}/catalog.json");
        return JsonSerializer.Deserialize<List<PageModelIngest>>(s)!
            .SelectMany(s => s.threads)
            .ToArray();
    });
        
    [HttpGet("boards")]
    public async Task<ActionResult> Get() => Ok((await BoardsCache).Select(s => new BoardInfoExport(s)));

    [HttpGet("catalog")]
    public async Task<ActionResult> Get(string? board)
    {
        if (board == null)
            return BadRequest("Provided parameter is null");
        if (!board.All(char.IsLetterOrDigit))
            return BadRequest("Provided parameter is not alphanumeric");
        return Ok((await CatalogCache[board]).Select(s => new ThreadModelExport(s, board)));
    }

    [HttpGet("image")]
    public async Task<ActionResult> Get(string? board, string? image)
    {
        if (board == null || image == null)
            return BadRequest("Provided parameter is null");
        if (!board.All(char.IsLetterOrDigit) || !image.All(char.IsLetterOrDigit))
            return BadRequest("Provided parameter is not alphanumeric");
        Stream s;
        try
        {
            s = await Shared.Hc.GetStreamAsync($"https://i.4cdn.org/{board}/{image}s.jpg");
        }
        catch (HttpRequestException e)
        {
            if (e.StatusCode == null)
            {
                Console.WriteLine($"Something went wrong while fetching a 4cdn image\n{e}");
                return NotFound();
            }
            return StatusCode((int)e.StatusCode);
        }
        return new FileStreamResult(s, "image/jpeg");
    }
}