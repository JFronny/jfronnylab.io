﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers;

[ApiController]
[Route("jfapi/Chan", Name = "Chan v0")]
public class ChanController0 : ControllerBase
{
    private readonly ChanController1 _next;
    public ChanController0() => _next = new ChanController1();

    [HttpGet("boards")]
    public async Task<ActionResult> Get() => Ok(await ChanController1.BoardsCache);

    [HttpGet("catalog")]
    public async Task<ActionResult> Get(string? board)
    {
        if (board == null)
            return BadRequest("Provided parameter is null");
        if (!board.All(char.IsLetterOrDigit))
            return BadRequest("Provided parameter is not alphanumeric");
        return Ok(await ChanController1.CatalogCache[board]);
    }

    [HttpGet("image")]
    public Task<ActionResult> Get(string board, string image) => _next.Get(board, image);
}