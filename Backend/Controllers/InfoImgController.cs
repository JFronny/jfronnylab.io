using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using Backend.Model.Ingest;
using Backend.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using SixLabors.Fonts;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Drawing.Processing;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;

namespace Backend.Controllers;

[ApiController]
[Route("jfapi/[controller]")]
public class InfoImgController : ControllerBase
{
    [HttpGet("image.jpg")]
    public async Task<ActionResult> Get()
    {
        Configuration cfg = Configuration.Default.Clone();
        if (!SystemFonts.Families.Any())
            return Problem("No font installed");
        Font f = SystemFonts.Families.First().CreateFont(10);
        RichTextOptions to = new(f)
        {
            KerningMode = KerningMode.Standard
        };

        List<string> l = [];
        await foreach (string line in GetLines()) l.Add(line);
        if (!l.Any())
            return Problem("No text to get");

        using Image<Rgba32> image = new((int)l.Select(s => TextMeasurer.MeasureSize(s, to).Width).MaxBy(s => s), (int)(l.Count * f.Size));
            
        image.Mutate(cfg, context =>
        {
            context.Clear(Color.Black);
            to.Origin = new Vector2(0, -f.Size);
            Vector2 o = new(0, f.Size);
            foreach (string line in l)
            {
                to.Origin += o;
                context.DrawText(to, line, Color.White);
            }
        });

        MemoryStream ms = new();
        await image.SaveAsJpegAsync(ms);
        ms.Position = 0;
        return new FileStreamResult(ms, "image/jpeg");
    }

    private async IAsyncEnumerable<string> GetLines()
    {
        StringValues sv = Request.Headers["User-Agent"];
        if (sv.Count == 0)
            yield return "User Agent" + CouldNotBeRead;
        else
        {
            ClientInfo c = (await _regexesYaml).Parse(sv[0]!);
            yield return $"Browser: {c.UA.Family} {c.UA.Major}.{c.UA.Minor}";
            yield return
                $"Device: {c.Device.Brand} {c.Device.Family} {c.Device.Model}{(c.Device.IsSpider ? " (spider)" : "")}";
            yield return $"OS: {c.OS.Family} {c.OS.Major}.{c.OS.Minor}";
        }
        string? culture = Request.GetTypedHeaders()
            .AcceptLanguage.MaxBy(x => x.Quality ?? 1)?.Value.ToString();
        yield return culture == null
            ? "Language" + CouldNotBeRead
            : "Language: " + new CultureInfo(culture).EnglishName;
        string? ip = Request.Headers["X-Real-IP"];
        if (string.IsNullOrEmpty(ip)) ip = Request.HttpContext.Connection.RemoteIpAddress!.ToString();
        IpInfo.IpInfoData info = await IpInfo.Get(ip);
        yield return $"IP: {ip} ({info.Hostname})" + (info.Privacy == null ? "" :
            (info.Privacy.Vpn ? " (vpn)" : "") +
            (info.Privacy.Proxy ? " (proxy)" : "") +
            (info.Privacy.Tor ? " (tor)" : "") +
            (info.Privacy.Relay ? " (relay)" : "") +
            (info.Privacy.Hosting ? " (hosting)" : "")
            );
        if (info.City != null) yield return $"City: {info.PostalCode} {info.City}, {info.Region}, {info.Country}";
        if (info.Coordinates != null) yield return $"Coordinates: {info.Coordinates}";
        if (info.Timezone != null) yield return $"Timezone: {info.Timezone}";
        if (info.Organization != null) yield return $"Organization: {info.Organization}";
        if (info.Company != null) yield return $"Company: {info.Company.Type}: {info.Company.Name} ({info.Company.Domain})";
    }

    private readonly AsyncLazy<Parser> _regexesYaml = new(async () =>
    {
        await using Stream stream =
            await Shared.Hc.GetStreamAsync("https://github.com/ua-parser/uap-core/raw/master/regexes.yaml");
        using StreamReader reader = new(stream);
        return Parser.FromYaml(await reader.ReadToEndAsync());
    });
    private const string CouldNotBeRead = " could not be read";
}