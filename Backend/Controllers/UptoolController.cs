using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Backend.Model;
using Backend.Util;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace Backend.Controllers;

[ApiController]
[Route("jfapi/[controller]")]
public class UptoolController : ControllerBase
{
    private readonly AsyncLazyExpiring<UpToolMeta> _metaProvider = new(async () =>
    {
        XElement el = XElement.Parse(await Shared.Hc.GetStringAsync("https://gitlab.com/api/v4/projects/uptool%2FUpTool2/jobs/artifacts/master/raw/meta.xml?job=uptool"));
        return new UpToolMeta(
            new UpToolMeta.File(el.Element("File")!.Value, el.Element("Hash")!.Value, "\"Portable\""),
            new UpToolMeta.File(el.Element("Installer")!.Value, el.Element("InstallerHash")!.Value, "CLI"),
            new UpToolMeta.File(el.Element("InstallerGui")!.Value, el.Element("InstallerGuiHash")!.Value, "GUI"),
            el.Element("Version")!.Value);
    });
        
    [HttpGet]
    public async Task<ActionResult> Get()
    {
        string html = @"<html>
<head>
    <title>UpTool info</title>
    <link rel=""stylesheet"" href=""https://jfronny.gitlab.io/style.css"">
</head>
<body class=""embedBody"">
<table>
    <thead>
        <tr>
            <th scope=""col"">Type</th>
            <th scope=""col"">Link</th>
            <th scope=""col"">Hash</th>
        </tr>
    </thead>
    <tbody>";
        UpToolMeta meta = await _metaProvider;
        if (Request.Headers[HeaderNames.UserAgent].Contains("Windows"))
        {
            html += Tr(meta.Version, meta.InstallerGui, true);
            html += Tr(meta.Version, meta.InstallerCli, false);
        }
        else
        {
            html += Tr(meta.Version, meta.InstallerCli, true);
            html += Tr(meta.Version, meta.InstallerGui, false);
        }
        html += Tr(meta.Version, meta.Portable, false);
        html += @"
    </tbody>
</table>
</body>
</html>";
            
        return new FileStreamResult(new MemoryStream(Encoding.UTF8.GetBytes(html)), "text/html");
    }

    private string Tr(string version, UpToolMeta.File file, bool recommended) =>
        $@"
        <tr>
            <th scope=""row"">{file.Name + (recommended ? " (recommended)" : "")}</th>
            <td><a type=""button"" href=""{file.Url}"">{version}</a></td>
            <td>{file.Hash}</td>
        </tr>";
}